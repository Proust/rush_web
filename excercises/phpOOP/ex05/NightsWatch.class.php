<?php
Class NightsWatch
{
	public $watch = array();
	public function recruit($name){
		if ($name instanceof IFighter){
			$this->watch[] = $name;
		}
 	}
	public function fight(){
		foreach ($this->watch as $elem){
			$elem->fight();
		}
	}
}
?>