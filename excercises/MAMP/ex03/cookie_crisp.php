<?php 
$action = $_GET["action"];
$name = $_GET["name"];
$value = $_GET["value"];

if ($action == "set") 
{
	setcookie($name, $value, time() + (86400 * 30));
}
else if ($action == "get") 
{
	if ($_COOKIE[$name])
	{
  		echo "$_COOKIE[$name]";
	} 	
}
else if ($action == "del") 
{
	setcookie($name, '', time() - 3600);
}
?>