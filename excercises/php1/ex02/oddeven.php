#!/usr/bin/php
<?php
	$stdin = fopen("php://stdin", "r");
	while ($stdin)
	{
		echo "Enter a number: ";
		if (($number = trim(fgets($stdin))) == NULL)
		{
			break;
		}
		if (is_numeric($number))
		{	
			if ($number % 2 == 0) 
			{
				print("The number $number is even\n");
			}
			else
			{
				print("The number $number is odd\n");
			}
		}
		else
		{
			print("'$number' is not a number\n");
		}
	}
	fclose($stdin);
	print ("\n");
?>
