#!/usr/bin/php
<?php
	date_default_timezone_set('Europe/Paris');
	if ($argc > 1)
	{
		if (preg_match("/^(:\b[lL]undi\b|\b[mM]ardi\b|\b[mM]ercredi\b|\b[jJ]eudi\b|\b[vV]endredi\b|\b[sS]amedi\b|\b[dD]imanche\b) [0-9][0-9]? (:\b[jJ]anvier\b|\b[fF]évrier\b|\b[mM]ars\b|\b[aA]vril\b|\b[mM]ai\b|\b[Juin]\b|\b[jJ]uillet\b|\b[aA]oût\b|\b[sS]eptembre\b|\b[oO]ctobre\b|\b[nN]ovembre\b|\b[dD]écembre\b) [0-9][0-9][0-9][0-9] [0-9][0-9]:[0-9][0-9]:[0-9][0-9]$/", $argv[1]) == TRUE)
		{
			$str = explode(" ", $argv[1]);
			
			$date[0] = $str[1];
			$date[1] = $str[2];
			$date[2] = $str[3];
			$date[3] = $str[4];
			$time = explode(":", $str[4]);
			$find = array('/[jJ]anvier/', '/[fF]évrier/', '/[mM]ars/', '/[aA]vril/', '/[mM]ai/', '/[Juin]/', '/[jJ]uillet/', '/[aA]oût/', '/[sS]eptembre/', '/[oO]ctobre/', '/[nN]ovembre/', '/[dD]écembre/');
			$replace = array('1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12');
			$month = preg_replace($find, $replace, $date[1]);

			$result = mktime($time[0], $time[1], $time[2], $month, $date[0], $date[2], -1);
			echo "$result\n";
		}
		else
		{
			echo "Wrong Format\n";
		}
	}
?>