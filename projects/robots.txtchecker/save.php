<?php

session_start();

 require_once'PHPExcel.php';
$tablexcel = new PHPExcel();
$tablexcel->setActiveSheetIndex(0);
$sheet = $tablexcel->getActiveSheet();
$sheet->setTitle("robots.txt");
$tablexcel->getProperties()->setCreator("Anastasiia Pietushkova")
							 ->setLastModifiedBy("Anastasiia Pietushkova")
							 ->setTitle("robots.txt")
							 ->setSubject("sitechecker")
							 ->setDescription("checking robots.txt file")
							 ->setKeywords("office robots.txt openxml php")
							 ->setCategory("Check result file");
$header = "a1:e1";
$sheet->getStyle($header)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$sheet->getStyle($header)->getFill()->getStartColor()->setRGB("9bbdc7");
$sheet->getStyle("a1:e19")->getFont()->setSize("8");

$empty = ["a2:e2","a5:e5","a8:e8","a11:e11","a14:e14","a17:e17"];

foreach ($empty as $cell)
{
    $sheet->getStyle($cell)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $sheet->getStyle($cell)->getFill()->getStartColor()->setRGB("dedede");
  
}

foreach($sheet->getRowDimensions() as $rd) { 
    $rd->setRowHeight(-1); 
}
$wide = [
    'a3:a4','a6:a7','a9:a10','a12:a13','a15:a16','a18:a19',
    'b3:b4','b6:b7','b9:b10','b12:b13','b15:b16','b18:b19',
    'c3:c4','c6:c7','c9:c10','c12:c13','c15:c16','c18:c19'
];
foreach($wide as $cell) {
    $sheet->mergeCells($cell);
}
$sheet->getStyle('c3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle('c6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle('c9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle('c12')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle('c15')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle('c18')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle('a3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle('a6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle('a9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle('a12')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle('a15')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle('a18')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle('a1:e19')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->getRowDimension("3")->setRowHeight("16");
$sheet->getRowDimension("4")->setRowHeight("16");
$sheet->getRowDimension("6")->setRowHeight("16");
$sheet->getRowDimension("7")->setRowHeight("16");
$sheet->getRowDimension("9")->setRowHeight("16");
$sheet->getRowDimension("10")->setRowHeight("16");
$sheet->getRowDimension("12")->setRowHeight("16");
$sheet->getRowDimension("13")->setRowHeight("16");
$sheet->getRowDimension("15")->setRowHeight("16");
$sheet->getRowDimension("16")->setRowHeight("16");
$sheet->getRowDimension("18")->setRowHeight("16");
$sheet->getRowDimension("19")->setRowHeight("16");
$sheet->getColumnDimension("a")->setWidth(6);
$sheet->getColumnDimension("b")->setWidth(41);
$sheet->getColumnDimension("c")->setWidth(8);
$sheet->getColumnDimension("d")->setWidth(10);
$sheet->getColumnDimension("e")->setWidth(45);
$sheet->getStyle('a1:e19')->getAlignment()->setWrapText(true); 

$sheet->setCellValue("a1", "№");
$sheet->setCellValue("b1", "Название проверки");
$sheet->setCellValue("c1", "Статус");
$sheet->setCellValue("d1", "");
$sheet->setCellValue("e1", "Текущее состояние");

$sheet->setCellValue("a3", "1");
$sheet->setCellValue("b3", "Проверка наличия файла robots.txt");
$sheet->setCellValue("d3", "Состояние");
$sheet->setCellValue("d4", "Рекомендации");
if ($_SESSION['code_robot'] >= 300){   
    $sheet->getStyle('c3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $sheet->getStyle('c3')->getFill()->getStartColor()->setRGB('e3725d');
    $sheet->setCellValue("c3", "Ошибка");
    $sheet->setCellValue("e3", "Файл robots.txt отсутствует");
    $sheet->setCellValue("e4", "Программист: создать сайт robots.tst и разместить его на сайте");
}
else{
    $sheet->getStyle('c3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $sheet->getStyle('c3')->getFill()->getStartColor()->setRGB('98e086');
    $sheet->setCellValue("c3", "ОК");
    $sheet->setCellValue("e3", "Файл robots.txt присутствует");
    $sheet->setCellValue("e4", "Доработки не требуются");
}

$sheet->setCellValue("a6", "6");

$sheet->setCellValue("b6", "Проверка указания директивы Host");
$sheet->setCellValue("d6", "Состояние");
$sheet->setCellValue("d7", "Рекомендации");
if ($_SESSION['count_host'] == 0){   
     $tablexcel->getActiveSheet()->getRowDimension("7")->setRowHeight("50");
    $sheet->getStyle('c6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $sheet->getStyle('c6')->getFill()->getStartColor()->setRGB('e3725d');
    $sheet->setCellValue("c6", "Ошибка");
    $sheet->setCellValue("e6", "В файле robots.txt не указана директива Host");
    $sheet->setCellValue("e7", "Программист: Для того, чтобы поисковые системы знали, какая версия сайта является основным зеркалом, необходимо прописать адрес основного зеркала в директиве Host. В данный момент это не прописано.  Необходимо добавить в файл robots.txt директиву 
Host. Директива Host задается в файле 1 раз, после всех правил");
}
else{
    $tablexcel->getActiveSheet()->getRowDimension("7")->setRowHeight("20");
    $sheet->getStyle('c6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $sheet->getStyle('c6')->getFill()->getStartColor()->setRGB('98e086');
    $sheet->setCellValue("c6", "ОК");
    $sheet->setCellValue("e6", "Директива Host указана");
    $sheet->setCellValue("e7", "Доработки не требуются");
}

$sheet->setCellValue("a9", "8");
$sheet->setCellValue("b9", "Проверка количества директив Host, прописанных в файле");
$sheet->setCellValue("d9", "Состояние");
$sheet->setCellValue("d10", "Рекомендации");
if ($_SESSION['count_host'] == 0){   
    $sheet->getStyle('c9')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $sheet->getStyle('C9')->getFill()->getStartColor()->setRGB('e3725d');
    $sheet->setCellValue("c9", "Ошибка");
    $sheet->setCellValue("e9", "Проверка невозможна, т.к. директива Host отсутствует");
    $sheet->setCellValue("e10", "Программист: прописать директиву Host");
}
else if($_SESSION['count_host'] > 1){
    $tablexcel->getActiveSheet()->getRowDimension("10")->setRowHeight("40");
    $sheet->getStyle('c9')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $sheet->getStyle('c9')->getFill()->getStartColor()->setRGB('e3725d');
    $sheet->setCellValue("c9", "Ошибка");
    $sheet->setCellValue("e9", "В файле прописано несколько директив Host");
    $sheet->setCellValue("e10", "Программист: Директива Host должна быть указана в файле только 1 раз. Необходимо удалить все дополнительные директивы Host и оставить только1, корректную и существующую основному зеркалу сайта");
}
else {
    $sheet->getStyle('c9')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $sheet->getStyle('c9')->getFill()->getStartColor()->setRGB('98e086');
    $sheet->setCellValue("c9", "ОК");
    $sheet->setCellValue("e9", "Директива Host указана");
    $sheet->setCellValue("e10", "Доработки не требуются");
}

$sheet->setCellValue("a12", "10");
$sheet->setCellValue("b12", "Проверка размера файла robots.txt");
$sheet->setCellValue("d12", "Состояние");
$sheet->setCellValue("d13", "Рекомендации");
if ($_SESSION['size'] > 32){   
     $tablexcel->getActiveSheet()->getRowDimension("13")->setRowHeight("30");
    $sheet->getStyle('c12')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $sheet->getStyle('c12')->getFill()->getStartColor()->setRGB('e3725d');
    $sheet->setCellValue("c12", "Ошибка");
    $sheet->setCellValue("e12", "Размер файла robots.txt составляет " .$_SESSION['size']." Кб, что превышает допустимую норму");
    $sheet->setCellValue("e13", "Программист: Максимально допустимый размер файлов robots.txt составляет 32 Кб. Необходимо отредактировать файл robots.txt таким образом, чтобы его размер не превышал 32 Кб");
}
else{
    $tablexcel->getActiveSheet()->getRowDimension("12")->setRowHeight("20");
    $sheet->getStyle('c12')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $sheet->getStyle('c12')->getFill()->getStartColor()->setRGB('98e086');
    $sheet->setCellValue("c12", "ОК");
    $sheet->setCellValue("e12", "Размер файла robots.txt составляет " .$_SESSION['size']." Кб, что находится в пределах допустимой нормы");
    $sheet->setCellValue("e13", "Доработки не требуются");
}

$sheet->setCellValue("a15", "11");
$sheet->setCellValue("b15", "Проверка указания директивы Sitemap");
$sheet->setCellValue("d15", "Состояние");
$sheet->setCellValue("d16", "Рекомендации");
if ($_SESSION['count_Sitemap'] == 0){   
    $sheet->getStyle('c15')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $sheet->getStyle('c15')->getFill()->getStartColor()->setRGB('e3725d');
    $sheet->setCellValue("c15", "Ошибка");
    $sheet->setCellValue("e15", "В файле robots.txt не указана директива Sitemap");
    $sheet->setCellValue("e16", "Программист: Добавить в файл robots.txt директиву Sitemap");
}
else{
    $sheet->getStyle('c15')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $sheet->getStyle('c15')->getFill()->getStartColor()->setRGB('98e086');
    $sheet->setCellValue("c15", "ОК");
    $sheet->setCellValue("e15", "Директива Sitemap указана");
    $sheet->setCellValue("e16", "Доработки не требуются");
}

$sheet->setCellValue("a18", "12");
$sheet->setCellValue("b18", "Проверка кода ответа сервера для файла robots.txt");
$sheet->setCellValue("d18", "Состояние");
$sheet->setCellValue("d19", "Рекомендации");
if ($_SESSION['code_robot'] >= 300){   
    $tablexcel->getActiveSheet()->getRowDimension("19")->setRowHeight("40");
    $sheet->getStyle('c18')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $sheet->getStyle('c18')->getFill()->getStartColor()->setRGB('e3725d');
    $sheet->setCellValue("c18", "Ошибка");
    $sheet->setCellValue("e18", "При обращению к файлу robots.txt сервер возвращает код ответа ".$_SESSION['code_robot']);
    $sheet->setCellValue("e19", "Программист: Файл robots.txt должен отдавать код ответа 200, иначе файл не будет обрабатываться. Необходимо настроить сайт таким образом, чтобы при обращении к файлу robots.txt сервер возвращает код ответа 200");
}
else{
    $sheet->getStyle('c18')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $sheet->getStyle('c18')->getFill()->getStartColor()->setRGB('98e086');
    $sheet->setCellValue("c18", "ОК");
    $sheet->setCellValue("e18", "Файл robots.txt отдаёт код ответа сервера 200");
    $sheet->setCellValue("e19", "Доработки не требуются");
}
    if(preg_match('/(?<=\.|\/)([A-z]+[^www])(?=\.)/', $_SESSION['name'],$m)) 
                        $name=$m[1]; 
$savename = $name.".xls";
$tablexcel->setActiveSheetIndex(0);
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$savename.'');
header('Cache-Control: max-age=0');
header ( "Cache-Control: no-cache, must-revalidate" );
header ( "Pragma: no-cache" );
$objWriter = PHPExcel_IOFactory::createWriter($tablexcel, 'Excel5');
$objWriter->save('php://output');

session_destroy();
exit;
    
?>